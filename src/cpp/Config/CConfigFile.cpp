#include "Config/CConfigFile.hpp"
#include "Exceptions.hpp"

const int ConfigFile::sm_defaultNegValue = -1;

const std::string ConfigFile::sm_configuration = "configuration";

const std::string ConfigFile::sm_outputFolderPath = "output_folder";

const std::string ConfigFile::sm_imagesFolder = "images_folder";

const std::string ConfigFile::sm_executorType = "executor_type";
const std::string ConfigFile::sm_unknownExecutorType = "Unknown executor type";

const std::string ConfigFile::sm_detectorType = "detector_type";
const std::string ConfigFile::sm_extractorType = "extractor_type";
const std::string ConfigFile::sm_matcherType = "matcher_type";

const std::string ConfigFile::sm_clusterCount = "cluster_count";

const std::string ConfigFile::sm_termCrit = "term_criteria";
const std::string ConfigFile::sm_eps = "eps";
const std::string ConfigFile::sm_maxCount = "max_count";

const std::string ConfigFile::sm_attemps = "attemps";

const std::string ConfigFile::sm_vocabularyFile = "vocabulary_file";

EnumParser< ExecutorVals > ConfigFile::sm_executorsParser;

std::string ConfigFile::emptyStringOrGetFailedMsg(const std::string& pathIn)
{
    return ("Reading failed or " + pathIn + " is empty");
}

std::string ConfigFile::negValueOrGetFailedMsg(const std::string& pathIn)
{
    return ("Reading failed or " + pathIn + " < 0");
}

// TODO: try checkElement with pathIn as parameter and specialisation of string and vector to chec emptyness ???
template< class T > void ConfigFile::checkEmptyness(const T& arrIn, const std::string& pathIn)
{
    if (arrIn.empty())
    {
        throw ConfigFileException(emptyStringOrGetFailedMsg(pathIn));
    }
}

template< class T > void ConfigFile::checkNegativity(T valIn, const std::string& pathIn)
{
    if (valIn < 0)
    {
        throw ConfigFileException(negValueOrGetFailedMsg(pathIn));
    }
}

template< class T > void ConfigFile::storeElement(const std::string& elemKeyIn, const T& elemValIn)
{
    m_configMap.emplace(elemKeyIn, make_unique< ConfigItem< T > >(elemValIn));
}

cv::TermCriteria ConfigFile::getTermCriteria(const PTree& pTreeIn)
{
    if (pTreeIn.empty())
    {
        throw EmptyPTreeError("TermCriteria");
    }
    double eps = pTreeIn.get< double >(sm_eps);
    checkNegativity< double >(eps, sm_eps);
    int maxIter = pTreeIn.get< int >(sm_maxCount);
    checkNegativity< int >(maxIter, sm_maxCount);
    return cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, maxIter, eps);
}

ConfigFile::ConfigFile(const std::string& nameIn)
{
    PTree tmpPT;
    try
    {
        xml_parser::read_xml(nameIn, tmpPT);
    }
    catch (boost::exception& e)
    {
        throw ConfigFileException("Config file ill formatted");
    }

    if (tmpPT.empty())
    {
        throw EmptyPTreeError("Config");
    }

    tmpPT = tmpPT.get_child(sm_configuration);
    if (tmpPT.empty())
    {
        throw EmptyPTreeError("No configuration body");
    }

    storeElement< std::string >(sm_outputFolderPath, tmpPT.get< std::string >(sm_outputFolderPath));
    checkEmptyness< std::string >(*m_configMap[sm_outputFolderPath], sm_outputFolderPath);

    storeElement< fs::path >(sm_imagesFolder, tmpPT.get< fs::path >(sm_imagesFolder));
    checkEmptyness< fs::path >(*m_configMap[sm_imagesFolder], sm_imagesFolder);

    storeElement< std::string >(sm_detectorType, tmpPT.get< std::string >(sm_detectorType));
    checkEmptyness< std::string >(*m_configMap[sm_detectorType], sm_detectorType);

    storeElement< std::string >(sm_extractorType, tmpPT.get< std::string >(sm_extractorType));
    checkEmptyness< std::string >(*m_configMap[sm_extractorType], sm_extractorType);

    storeElement< std::string >(sm_matcherType, tmpPT.get< std::string >(sm_matcherType));
    checkEmptyness< std::string >(*m_configMap[sm_matcherType], sm_matcherType);

    storeElement< std::string >(sm_executorType, tmpPT.get< std::string >(sm_executorType));
    checkEmptyness< std::string >(*m_configMap[sm_executorType], sm_executorType);
    switch (sm_executorsParser.parseToEnum(*m_configMap[sm_executorType]))
    {
        case TRAIN_BOW:
        {
            storeElement< int >(sm_clusterCount, tmpPT.get< int >(sm_clusterCount));
            checkNegativity< int >(*m_configMap[sm_clusterCount], sm_clusterCount);

            storeElement< cv::TermCriteria >(sm_termCrit, getTermCriteria(tmpPT.get_child(sm_termCrit)));

            storeElement< int >(sm_attemps, tmpPT.get< int >(sm_attemps));
            checkNegativity< int >(*m_configMap[sm_attemps], sm_attemps);
        }
        break;

        case TEST_BOW:
        {
            storeElement< std::string >(sm_vocabularyFile, tmpPT.get< std::string >(sm_vocabularyFile));
            checkEmptyness< std::string >(*m_configMap[sm_vocabularyFile], sm_vocabularyFile);
        }
        break;

        default:
            throw ConfigFileException(sm_unknownExecutorType);
    }
}

ConfigFile::~ConfigFile()
{

}

std::string ConfigFile::getOutputFolder()
{
    return *m_configMap[sm_outputFolderPath];
}

std::string ConfigFile::getDescriptorsExtractorName()
{
    return *m_configMap[sm_extractorType];
}

std::string ConfigFile::getDescriptorsMatcherName()
{
    return *m_configMap[sm_matcherType];
}

std::string ConfigFile::getFeaturesDetectorName()
{
    return *m_configMap[sm_detectorType];
}

int ConfigFile::getExecutorType()
{
    return sm_executorsParser.parseToEnum(*m_configMap[sm_executorType]);
}

int ConfigFile::getClusterCount()
{
    return *m_configMap[sm_clusterCount];
}

cv::TermCriteria ConfigFile::getTermCriteria()
{
    return *m_configMap[sm_termCrit];
}

int ConfigFile::getAttemps()
{
    return *m_configMap[sm_attemps];
}

fs::path ConfigFile::getImagesFolder()
{
    return *m_configMap[sm_imagesFolder];
}

std::string ConfigFile::getVocabularyFileName()
{
    return *m_configMap[sm_vocabularyFile];
}

#include "Config/CConfigFile.hpp"
#include "Exceptions.hpp"
#include "Executors/Detector/CBOWDetector.hpp"
#include "Executors/Learner/CBOWLearner.hpp"
#include "Tools/InOut/CInOut.hpp"
#include "Tools/Logger/CLogger.hpp"

#include <boost/log/attributes/current_process_id.hpp>
#include <boost/log/attributes/current_process_name.hpp>
#include <boost/log/core.hpp>

/** \brief Display usage of the application */
static void showUsage(std::string name)
{
  std::cerr << "Usage: " << name << " <option(s)> SOURCES" << std::endl
    << "  -h,--help                Show this help message" << std::endl
    << "  -c,--config CONFIG FILE  Specify the config file's name and path" << std::endl
    << std::endl;
}

static void initRunnerPtr(std::shared_ptr< IRunner >& runnerPtrInOut, ConfigFile& cfgIn)
{
    cv::initModule_nonfree();
    
    switch (cfgIn.getExecutorType())
    {
        case TRAIN_BOW:
            runnerPtrInOut.reset(new BOWLearner(cfgIn)); // TODO: rewiew ConfigFile ?
            break;
        case TEST_BOW:
            runnerPtrInOut.reset(new BOWDetector(cfgIn));
            break;
        default:
            throw ExternalException("Unknown runner"); // TODO: add the proper exception
    }
}

int main(int argc, char **argv)
{
    Logger::initLogging();
    Logger::initFilesList();

    boost::log::add_common_attributes();
    boost::log::core::get()->add_global_attribute("Executable", boost::log::attributes::current_process_name());

    BoostLogger lg = setClassNameAttribute("Main");

    if (argc == 3)
    {
        std::string opt = argv[1];
        if (opt == "-c" || opt == "--config")
        {
            std::string configFileNameNPath = argv[2];
            try
            {
                ConfigFile cfg(configFileNameNPath);
                std::shared_ptr< IRunner > runnerPtr;
                initRunnerPtr(runnerPtr, cfg);
                runnerPtr->execute();
            }
            catch (ExternalException& ee)
            {
                BOOST_LOG_SEV(lg, Logger::SeverityLevels::error) << ee.what();
                return EXTERNAL_EXCEPTION;
            }
            catch (std::exception& e)
            {
                BOOST_LOG_SEV(lg, Logger::SeverityLevels::error) << e.what();
                return CPP_EXCEPTION;
            }
            catch (...)
            {
                BOOST_LOG_SEV(lg, Logger::SeverityLevels::error) << "Unknown exception";
                return UNKNOWN_EXCEPTION;
            }
        }
        else
        {
            showUsage(argv[0]);
            return HELP;
        }
    }
    else
    {
        showUsage(argv[0]);
        return HELP;
    }

    return NO_ERRORS;
}

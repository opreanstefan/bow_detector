#include "Exceptions.hpp"
#include "Tools/InOut/CInOut.hpp"

char InOut::getMinuscule(char chIn)
{
  if ('A' <= chIn && chIn <= 'Z')
    return chIn - ('A' - 'a');
  return chIn;
}

PathsVector InOut::getImagesPaths(const fs::path& folderPathIn, const std::string& extensionIn,
                                  bool isCaseSensitiveExtensionIn)
{
    std::string goodext;
    if (extensionIn.empty())
    {
        goodext = ".jpg";
    }
    else if (extensionIn[0] != '.')
    {
        goodext = "." + extensionIn;
    }
    else
    {
        goodext = extensionIn;
    }

    if (!isCaseSensitiveExtensionIn)
        std::transform(goodext.begin(), goodext.end(), goodext.begin(), getMinuscule);

    PathsVector imagesNameRet;
    fs::directory_iterator end;
    for(fs::directory_iterator iter(folderPathIn); iter != end; ++iter)
    {
        if(!fs::is_regular_file(iter->status())) 
        {
            continue;
        }

        std::string extension = iter->path().extension().string();
        if (!isCaseSensitiveExtensionIn)
            std::transform(extension.begin(), extension.end(), extension.begin(), getMinuscule);

        if(extension == goodext) 
        {
            imagesNameRet.push_back(iter->path());
        }
    }

    return imagesNameRet;
}

void InOut::saveVocabulary(const fs::path& folderPathIn, const std::string& fileNameIn, const cv::Mat& vocabularyIn)
{
    fs::create_directories(folderPathIn);
    fs::path filePath = folderPathIn;
    filePath /= fileNameIn;
    cv::FileStorage fileStorage(filePath.c_str(), cv::FileStorage::WRITE);
    if (!fileStorage.isOpened())
    {
        fileStorage.release();
        throw OutputFileException(filePath.c_str());
    }
    fileStorage << "Vocabulary" << vocabularyIn;
    fileStorage.release();
}

cv::Mat InOut::readVocabulary(const std::string& vocabularyFileIn)
{
    cv::FileStorage fileStorage(vocabularyFileIn, cv::FileStorage::READ);
    if (!fileStorage.isOpened())
    {
        fileStorage.release();
        throw ReadFlieStorageExcreption(vocabularyFileIn);
    }
    cv::Mat vocRet;
    fileStorage["Vocabulary"] >> vocRet;
    return vocRet;
}

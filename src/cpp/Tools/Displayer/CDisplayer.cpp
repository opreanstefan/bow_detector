#include "Tools/Displayer/CDisplayer.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <random>

void Displayer::showImage(const cv::Mat& imgIn, const std::string& winNameIn)
{
    cv::imshow(winNameIn, imgIn);
    cv::waitKey(0);
}

void Displayer::showImageKP(const cv::Mat& imgIn, const KeyPointsVerctor& kptsIn, const std::string& winNameIn)
{
    cv::Mat displayed = imgIn.clone();
    std::default_random_engine dre;
    std::uniform_int_distribution< uchar > ud(20, 255);
    for (cv::KeyPoint kp : kptsIn)
    {
        cv::circle(displayed, kp.pt, 3, CV_RGB(ud(dre), ud(dre), ud(dre)), 3);
    }
    showImage(displayed, winNameIn);
}

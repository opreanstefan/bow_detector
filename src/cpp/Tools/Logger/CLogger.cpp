#include "Tools/Logger/CLogger.hpp"
#include "Types.hpp"
// #include "CDisplayer.hpp"
#include "Exceptions.hpp"

#include <boost/core/null_deleter.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/attributes/current_process_id.hpp>
#include <boost/log/attributes/current_process_name.hpp>
#include <boost/log/core/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/file.hpp>

typedef boost::log::sinks::synchronous_sink< boost::log::sinks::text_file_backend > SinkTextFileBakend;

BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", Logger::SeverityLevels)
BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)
BOOST_LOG_ATTRIBUTE_KEYWORD(threadid, "ThreadID", boost::log::attributes::current_thread_id)
BOOST_LOG_ATTRIBUTE_KEYWORD(classname, "ClassName", std::string)

std::list< std::string > Logger::sm_logFilesList;

const std::string Logger::sm_folderOfLogFiles = "/var/log/bow_detector/";

std::ostream& operator<<(std::ostream& strm, Logger::SeverityLevels level)
{
    static const char* strings[] =
    {
        "DEBUG",
        "INFO",
        "WARN",
        "ERROR"
    };

    if (static_cast< std::size_t >(level) < sizeof(strings) / sizeof(*strings))
        strm << strings[level];
    else
        strm << static_cast< int >(level);

    return strm;
}

BoostLogger setClassNameAttribute(const std::string& classNameIn)
{
    BoostLogger lg;
    lg.add_attribute("ClassName", boost::log::attributes::constant<std::string>(classNameIn));
    return lg;
}

void Logger::removeOldLogFiles()
{
    while (sm_logFilesList.size() > 2)
    {
        std::string fileToDelete = sm_logFilesList.front();
        if (fs::exists(fs::path(fileToDelete)))
        {
            fs::remove(fs::path(fileToDelete));
            sm_logFilesList.pop_front();
        }
        else
        {
            throw NoSuchLogFileException(fileToDelete);
        }
    }
}

bool Logger::compareAccessTime(const std::string& path1In, const std::string& path2In)
{
    return (fs::last_write_time(fs::path(path1In)) < fs::last_write_time(fs::path(path2In)));
}

void Logger::addNewLogFileToList(const std::string& logFilePathIn, bool sorting)
{
    if (std::find(sm_logFilesList.begin(), sm_logFilesList.end(), logFilePathIn) == sm_logFilesList.end())
    {
        sm_logFilesList.push_back(logFilePathIn);
        if (sorting)
        {
            sm_logFilesList.sort(compareAccessTime);
        }
        removeOldLogFiles();
    }
}

void Logger::addNewRemoveOldLogs(bool sorting)
{
    fs::path path(sm_folderOfLogFiles);
    fs::directory_iterator endDir;
    if (checkPath(path))
    { 
        for (fs::directory_iterator it(path); it != endDir; it++)
        {
            if (fs::is_regular_file(it->status()))
            {
                if (fs::extension(*it) == ".log")
                {
                    std::string fileToPush = it->path().string();
                    addNewLogFileToList(fileToPush, sorting);
                }
            }
        }
    }
}

bool Logger::checkPath(const boost::filesystem::path& pathIn)
{
    if (!fs::exists(pathIn))
    {
        return false;
    }
    if (!fs::is_directory(pathIn))
    {
        return false;
    }

    return true;
}

void Logger::openingHandler(boost::log::sinks::text_file_backend::stream_type& file)
{
    addNewRemoveOldLogs();
}

void Logger::initFilesList()
{
    addNewRemoveOldLogs(true);
}

void Logger::initLogging()
{
    // Create a backend
    boost::shared_ptr< SinkTextFileBakend > sink = boost::log::add_file_log(sm_folderOfLogFiles
        + "optimisations_%Y-%m-%d_%H-%M-%S.%N.log", boost::log::keywords::format = boost::log::expressions::stream
        << boost::log::expressions::attr< boost::log::attributes::current_process_name::value_type >("Executable")
        << "(" << boost::log::expressions::attr< boost::log::attributes::current_process_id::value_type >("ExeUID")
        << ") " << severity << "["
        << boost::log::expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f")
        << "] [" << boost::log::expressions::attr< boost::log::attributes::current_thread_id::value_type >("ThreadID")
        << "] " << classname << " - " << boost::log::expressions::smessage,
        boost::log::keywords::open_mode = (std::ios::out | std::ios::app),
        boost::log::keywords::rotation_size = 2 * 1024 * 1024, boost::log::keywords::auto_flush = true);

    sink->locked_backend()->set_open_handler(&openingHandler);

    boost::log::core::get()->set_filter(severity >= SeverityLevels::debug);
    boost::log::core::get()->add_sink(sink);

#ifdef DEBUGGING
    boost::shared_ptr< boost::log::sinks::text_ostream_backend > backend =
        boost::make_shared< boost::log::sinks::text_ostream_backend >();
    backend->add_stream(boost::shared_ptr< std::ostream >(&std::cout, boost::null_deleter()));

    // Enable auto-flushing after each log record written
    backend->auto_flush(true);

    // Wrap it into the frontend and register in the core.
    // The backend requires synchronization in the frontend.
    // for testing and printing log in sysout
    boost::shared_ptr< boost::log::sinks::synchronous_sink< boost::log::sinks::text_ostream_backend > > backend_sink(
        new boost::log::sinks::synchronous_sink< boost::log::sinks::text_ostream_backend >(backend));

    backend_sink->set_formatter(boost::log::expressions::stream
        // line id will be written in hex, 8-digits, zero-filled
        << boost::log::expressions::attr< boost::log::attributes::current_process_name::value_type >("Executable")
        << "(" << boost::log::expressions::attr< boost::log::attributes::current_process_name::value_type >("ExeUID")
        << ") " << severity << " ["
        << boost::log::expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f")
        << "] [" << boost::log::expressions::attr< boost::log::attributes::current_thread_id::value_type >("ThreadID")
        << "] " << classname << " - " << boost::log::expressions::smessage);
    boost::log::core::get()->add_sink(backend_sink); // for testing and printing log in sysout
#endif
}

#include "Executors/Learner/CBOWLearner.hpp"
#include "Exceptions.hpp"
#include "Tools/InOut/CInOut.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/nonfree/features2d.hpp>

BoostLogger BOWLearner::sm_lg = setClassNameAttribute("BOWLearner");

BOWLearner::BOWLearner(ConfigFile& configFileIn) :
m_detector(cv::FeatureDetector::create(configFileIn.getFeaturesDetectorName())),
m_extractor(cv::DescriptorExtractor::create(configFileIn.getDescriptorsExtractorName())),
m_matcher(cv::DescriptorMatcher::create(configFileIn.getDescriptorsMatcherName())),
m_bowTrainer(configFileIn.getClusterCount(), configFileIn.getTermCriteria(), configFileIn.getAttemps()),
m_outputFolder(configFileIn.getOutputFolder())
{
    if (m_detector.empty()) { throw EmptyDetectorError(configFileIn.getFeaturesDetectorName()); }
    if (m_extractor.empty()) { throw EmptyExtractorError(configFileIn.getDescriptorsExtractorName()); }
    if (m_matcher.empty()) { throw EmptyMatcherError(configFileIn.getDescriptorsMatcherName()); }

    BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Using: " << configFileIn.getFeaturesDetectorName()
        << " Detector, " << configFileIn.getDescriptorsExtractorName() << "  Extractor, "
        << configFileIn.getDescriptorsMatcherName() << " Matcher";

    BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Reading images and store their features' descriptors ...";
    extractBOWTrainer(InOut::getImagesPaths(configFileIn.getImagesFolder(), "png"));

    if (m_outputFolder[m_outputFolder.length() - 1] != '/')
    {
        m_outputFolder += '/';
    }
}

void BOWLearner::extractBOWTrainer(const PathsVector& imagesPathsIn)
{
    for (auto& imgPath : imagesPathsIn)
    {
        cv::Mat image = cv::imread(imgPath.string());
        if (image.empty())
        {
            throw ReadingImageWarning(imgPath.string());
        }
        KeyPointsVerctor keypoints;
        m_detector->detect(image, keypoints);
//         cv::Mat img;
//         cv::drawKeypoints(image, keypoints, img);
//         cv::imshow("blobs", img);
//         cv::waitKey();
        if (keypoints.empty())
        {
//             throw NoFeaturesFoundWarning(imgPath.string());
            BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::warning) << "No features fond on image " << imgPath.string();
            continue;
        }
        cv::Mat descriptors;
        m_extractor->compute(image, keypoints, descriptors);
        BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::debug) << imgPath.string() << " : " << keypoints.size()
            << " features";
        if (keypoints.empty())
        {
            continue;
        }
        if (descriptors.type() != CV_32FC1)
        {
            cv::Mat descriptorsFloat;
            descriptors.convertTo(descriptorsFloat, CV_32FC1);
            m_bowTrainer.add(descriptorsFloat);
        }
        else
        {
            m_bowTrainer.add(descriptors);
        }
    }
}

void BOWLearner::execute()
{
    BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Clustering the features ...";
    if (m_bowTrainer.getDescriptors().empty())
    {
        throw EmptyBOWError();
    }
    cv::Mat vocabulary = m_bowTrainer.cluster();

    BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Save vocabulary ...";
    std::string vocabularyFileName = "vocabulary.xml";
    InOut::saveVocabulary(m_outputFolder, vocabularyFileName, vocabulary);

    BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Vocabulary saved to " << m_outputFolder
        << vocabularyFileName;
}

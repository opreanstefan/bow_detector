#include "Executors/Detector/CBOWDetector.hpp"
#include "Tools/Displayer/CDisplayer.hpp"
#include "Tools/InOut/CInOut.hpp"

#include <opencv2/highgui/highgui.hpp>

BoostLogger BOWDetector::sm_lg = setClassNameAttribute("BOWDetector");

BOWDetector::BOWDetector(ConfigFile& configFileIn) :
m_detector(cv::FeatureDetector::create(configFileIn.getFeaturesDetectorName())),
m_extractor(cv::DescriptorExtractor::create(configFileIn.getDescriptorsExtractorName())),
m_matcher(cv::DescriptorMatcher::create(configFileIn.getDescriptorsMatcherName())),
m_bowDE(m_extractor, m_matcher),
m_imagesFolder(configFileIn.getImagesFolder())
{
    if (m_detector.empty()) { throw EmptyDetectorError(configFileIn.getFeaturesDetectorName()); }
    if (m_extractor.empty()) { throw EmptyExtractorError(configFileIn.getDescriptorsExtractorName()); }
    if (m_matcher.empty()) { throw EmptyMatcherError(configFileIn.getDescriptorsMatcherName()); }

    cv::Mat voc = InOut::readVocabulary(configFileIn.getVocabularyFileName());
    if (configFileIn.getDescriptorsMatcherName() != "FlannBased")
    {
        voc.convertTo(voc, CV_8UC1);
    }
    m_bowDE.setVocabulary(voc);
}

void BOWDetector::execute()
{
    PathsVector imgsPaths = InOut::getImagesPaths(m_imagesFolder, "jpg", true);
    for (fs::path pathIt : imgsPaths)
    {
        BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Detection on " << pathIt.string();
        cv::Mat image = cv::imread(pathIt.string());
        Displayer::showImage(image, "Image");
        KeyPointsVerctor keypoints;
        m_detector->detect(image, keypoints);
        Displayer::showImageKP(image, keypoints, "KeyPoints");
        cv::Mat descriptors;
        cv::Mat dmatched;
        std::vector< std::vector< int > > indexes;
        std::cout << "keypoints: " << keypoints.size() <<std::endl;
        m_bowDE.compute(image, keypoints, descriptors, &indexes, &dmatched);
        std::vector< std::size_t > counterMatches(keypoints.size(), 0);
        // TODO: create a function of it
        for (auto& v : indexes)
        {
            for (int idx : v)
            {
                counterMatches[idx]++;
            }
        }
        std::cout << "keypoints: " << keypoints.size() <<std::endl;
        std::cout << "indexes: " << indexes.size() <<std::endl;
        std::cout << "dmatched: " << dmatched.size() <<std::endl;
        Displayer::showImageKP(image, keypoints, "After match");
        BOOST_LOG_SEV(sm_lg, Logger::SeverityLevels::info) << "Found matches: " << dmatched.rows;
    }
}

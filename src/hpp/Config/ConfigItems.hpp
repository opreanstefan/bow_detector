#pragma once

#include <memory>
#include <string>

#include "Types.hpp"

class IConfigItem
{
public:
    virtual ~IConfigItem() {};

    virtual operator int() const { return 0; }
    virtual operator std::string() const { return std::string(); }
    virtual operator fs::path() const { return fs::path(); }
    virtual operator cv::Size() const { return cv::Size(); }
//     virtual operator cv::SVMParams () const { return cv::SVMParams(); }
    virtual operator cv::TermCriteria () const { return cv::TermCriteria(); }
};

/*********************************************************************************************************************/

template< typename T > class ConfigItem : public IConfigItem
{
private:
    T m_value;

public:
    ConfigItem< T >(const T& valueIn = 0) : m_value(valueIn) {}

    operator T() const
    {
        return m_value;
    }
};

// template<> class ConfigItem< cv::SVMParams > : public IConfigItem
// {
// private:
//     cv::SVMParams m_value;
// 
// public:
//     ConfigItem(const cv::SVMParams& valueIn) : m_value(valueIn) {}
// 
//     operator cv::SVMParams() const
//     {
//         return m_value;
//     }
// };

// template<> class ConfigItem< cv::ParamGrid > : public IConfigItem
// {
// private:
//     cv::ParamGrid m_value;
// 
// public:
//     ConfigItem(const cv::ParamGrid& valueIn) : m_value(valueIn) {}
// 
//     operator cv::ParamGrid() const
//     {
//         return m_value;
//     }
// };

/*********************************************************************************************************************/

template< typename T, typename ...Args > std::unique_ptr< T > make_unique(Args&& ...args)
{
    return std::unique_ptr< T >(new T(std::forward< Args >(args)...));
}

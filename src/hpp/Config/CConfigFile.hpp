#pragma once

#include <map>

#include "Config/ConfigItems.hpp"
#include "Exceptions.hpp"
#include "Tools/EnumParser/CEnumParser.hpp"
#include "Types.hpp"

class ConfigFile
{
private:
    static const int sm_defaultNegValue;

    static const std::string sm_configuration;
    static const std::string sm_outputFolderPath;

    static const std::string sm_imagesFolder;

    static const std::string sm_executorType;
    static const std::string sm_unknownExecutorType;

    static const std::string sm_detectorType;
    static const std::string sm_extractorType;
    static const std::string sm_matcherType;

    static const std::string sm_clusterCount;
    static const std::string sm_termCrit;
    static const std::string sm_eps;
    static const std::string sm_maxCount;
    static const std::string sm_attemps;

    static const std::string sm_vocabularyFile;

    static EnumParser< ExecutorVals > sm_executorsParser;

    std::map<std::string, std::unique_ptr< IConfigItem > > m_configMap;

    /** The function checks if the input array is empty and throws a ConfigFileException if so
     * 
     * @param arrIn : input array to check
     * 
     * @param pathIn : input path to create exception message
     */
    template< class T > void checkEmptyness(const T& arrIn, const std::string & pathIn);

    /** The function checks if the input value is negative and throws a ConfigFileException if so
    * 
    * @param valIn : input string to check
    * 
    * @param pathIn : input path to create exception message
    */
    template< class T > void checkNegativity(T valIn, const std::string & pathIn);

    /** The function creates the exception message for the empty string or get failed cases
    * 
    * @param pathIn : input path in the json file
    * 
    * @returns : output message
    */
    std::string emptyStringOrGetFailedMsg(const std::string& pathIn);

    /** The function creates the exception message for the negative values or get failed cases
    * @param pathIn : input path in the json file
    * 
    * @returns : output message
    */
    std::string negValueOrGetFailedMsg(const std::string& pathIn);

    /** The function stores an element in the configMap */
    template< class T > void storeElement(const std::string& elemKeyIn, const T& elemValIn);

    /** @returns a TermCriteria object from the PTree input */
    cv::TermCriteria getTermCriteria(const PTree& pTreeIn);

public:
    /** The constructor read the .json file from the path from the string given as input parameter and save it in the
    * map member
    *
    * @param name : input string containing the name and path of the config file
    */
    ConfigFile(const std::string & nameIn);
    ~ConfigFile();

    /** @returns the output folder path */
    std::string getOutputFolder();

    /** @returns the name of the features detector */
    std::string getFeaturesDetectorName();

    /** @returns the name of the descriptors extractor */
    std::string getDescriptorsExtractorName();

    /** @returns the name of the descriptors matcher */
    std::string getDescriptorsMatcherName();

    /** @returns the runner type */
    int getExecutorType();

    /** @return cluster count */
    int getClusterCount();

    /** @returns TermCriteria */
    cv::TermCriteria getTermCriteria();

    /** @returns the number of attemps to try for kmeans algorithm */
    int getAttemps();

    /** @returns the path to the images */
    fs::path getImagesFolder();

    /** @returns the path to the file that contains the vocabulary */
    std::string getVocabularyFileName();
};

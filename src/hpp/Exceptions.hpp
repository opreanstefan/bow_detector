#pragma once

#include <iostream>
#include <exception>

#include "Types.hpp"

class ExternalException : public std::exception
{
private:
    std::string m_msg;

public:
    ExternalException(const std::string& msgIn) : m_msg(msgIn) {}
    virtual ~ExternalException() throw() {}

    virtual const char* what() const throw() { return m_msg.c_str(); }
};

class ConfigFileException : public ExternalException
{
public:
    ConfigFileException(const std::string& msg) noexcept : ExternalException(msg) {}
};

class EnumParserError : public ExternalException
{
public:
    EnumParserError(const std::string& msgIn) noexcept : ExternalException(msgIn) {}
};

class EmptyBOWError : public ExternalException
{
public:
    EmptyBOWError() noexcept : ExternalException("No descriptors for clustering!") {}
};

class EmptyDetectorError : public ExternalException
{
public:
    EmptyDetectorError(const std::string& detectorTypeIn) noexcept
    : ExternalException("Detector " + detectorTypeIn + " not created. Be sure that the value is one of the following"
        + "{ \"FAST\", \"STAR\", \"SIFT\", \"SURF\", \"ORB\", \"BRISK\", \"MSER\", \"GFTT\", \"HARRIS\", \"Dense\", "
        + "\"SimpleBlob\" or combined: \"GridFAST\", \"GridSTAR\", \"GridSIFT\", \"GridSURF\", \"GridORB\", "
        + "\"GridBRISK\", \"GridMSER\", \"GridGFTT\", \"GridHARRIS\", \"GridDense\", \"GridSimpleBolb\", "
        + "\"PyramidFAST\", \"PyramidSTAR\", \"PyramidSIFT\", \"PyramidSURF\", \"PyramidORB\", \"PyramidBRISK\", "
        + "\"PyramidMSER\", \"PyramidGFTT\", \"PyramidHARRIS\", \"PyramidDense\", \"PyramidSimpleBolb\" }") {}
};

class EmptyExtractorError : public ExternalException
{
public:
    EmptyExtractorError(const std::string& extractorTypeIn) noexcept
    : ExternalException("Extractor " + extractorTypeIn + " not created. Be sure that the value is one of the "
        + "following: { \"SIFT\", \"SURF\", \"BRIEF\", \"BRISK\", \"ORB\", \"FREAK\" or combined: \"OpponentSIFT\", "
        + "\"OpponentSURF\", \"OpponentBRIEF\", \"OpponentBRISK\", \"OpponentORB\", \"OpponentFREAK\" }") {}
};

class EmptyMatcherError : public ExternalException
{
public:
    EmptyMatcherError(const std::string& matcherTypeIn) noexcept
    : ExternalException("Matcher " + matcherTypeIn + " not created. Be sure that the value is one of the following: "
        + "{ \"BruteForce\", \"BruteForce-L1\", \"BruteForce-L2\", \"BruteForce-SL2\", \"BruteForce-Hamming\", "
        + "\"BruteForce-Hamming(2)\", or \"FlannBased\" }") {}
};

class EmptyPTreeError : public ExternalException
{
public:
    EmptyPTreeError(const std::string& msgIn) noexcept : ExternalException(std::string("Empty ptree: ") + msgIn) {}
};

class NoSuchLogFileException : public ExternalException
{
public:
    NoSuchLogFileException(const std::string& fileNameIn) noexcept
    : ExternalException("The required solution (" + fileNameIn + ") has no implementation") {}
};

class OutputFileException : public ExternalException
{
public:
    OutputFileException(const std::string& fileNameIn) noexcept
    : ExternalException("Cannot write output file" + fileNameIn) {}
};

class ReadFlieStorageExcreption : public ExternalException
{
public:
    ReadFlieStorageExcreption(const std::string& filePathIn) noexcept
    : ExternalException("Cannot open file: " + filePathIn) {}
};

class ReadingImageWarning : public ExternalException
{
public:
    ReadingImageWarning(const std::string& imagePathIn) noexcept
    : ExternalException("Cannot read image: " + imagePathIn) {}
};

class UnknownLabelException : public ExternalException
{
public:
    UnknownLabelException(int labelIn) noexcept : ExternalException("Unknown label: " + std::to_string(labelIn)) {}
};

/*********************************************************************************************************************/

class Warnings : public std::exception
{
private:
    std::string m_msg;

public:
    Warnings(const std::string& msgIn) : m_msg(msgIn) {}
    virtual ~Warnings() throw() {}

    virtual const char* what() const throw() { return m_msg.c_str(); }
};

class NoFeaturesFoundWarning : public Warnings
{
public:
    NoFeaturesFoundWarning(const std::string& imagePathIn) noexcept
    : Warnings("No features found on " + imagePathIn){}
};

class NotADirectoryWarning : public Warnings
{
public:
    NotADirectoryWarning(const std::string& msgIn) noexcept : Warnings(msgIn + " is not a directory") {}
};

#pragma once

class IRunner
{
public:
    virtual ~IRunner() = 0;

    virtual void execute() = 0;
};

inline IRunner::~IRunner() {}

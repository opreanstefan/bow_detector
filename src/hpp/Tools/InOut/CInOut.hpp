#pragma once

#include "Types.hpp"

class InOut
{
private:
    static char getMinuscule(char chIn);

public:
    static PathsVector getImagesPaths(const fs::path& folderPathIn, const std::string& extensionIn,
                                      bool isCaseSensitiveExtensionIn = false);

    static void saveVocabulary(const fs::path& folderPathIn, const std::string& fileNameIn,
                               const cv::Mat& vocabularyIn);

    static cv::Mat readVocabulary(const std::string& vocabularyFileIn);
};

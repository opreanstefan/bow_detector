#pragma once

#include "Types.hpp"

class Displayer
{
public:
    static void showImage(const cv::Mat& imgIn, const std::string& winNameIn);
    static void showImageKP(const cv::Mat& imgIn, const KeyPointsVerctor& kptsIn, const std::string& winNameIn);
};
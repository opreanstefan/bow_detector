#pragma once

#include <list>
#include <string>

#include <boost/log/common.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/concept_check.hpp>

// TODO: try the singleton idea http://www.yolinux.com/TUTORIALS/C++Singleton.html
class Logger
{
private:
    static std::list< std::string > sm_logFilesList;

    static const std::string sm_folderOfLogFiles;

    /** the function adds the input log file to the list
     * 
     * @param logFilePathIn : input log file path
     */
    static void addNewLogFileToList(const std::string& logFilePathIn, bool sorting = false);

    /** The function adds new logs to new log file and remove the oltest ones to have always 2 log files */
    static void addNewRemoveOldLogs(bool sorting = false);

    /** @returns : if the input path is valid or not */
    static bool checkPath(const boost::filesystem::path& pathIn);

    /** @returns if the file with the first input path was more recent accesed than the file with the second input path
     */
    static bool compareAccessTime(const std::string& path1In, const std::string& path2In);

    /** The function remove the old log files and keeps just the most recent two */
    static void removeOldLogFiles();

    /** The function is calles at the end of each file and removes the old files keeping just the last two */
    static void openingHandler(boost::log::sinks::text_file_backend::stream_type& file);

public:
    enum SeverityLevels
    {
        debug,
        info,
        warning,
        error
    };

    /** The function sets the sm_logFilesList to contain the files if there are log files from earlier runs */
    static void initFilesList();

    /** The fuction is initializing the sinks and the formatter of the logger */
    static void initLogging();
};

typedef boost::log::sources::severity_logger< Logger::SeverityLevels > BoostLogger;

/** the overloaded << operator of the BoostLogger */
std::ostream& operator<< (std::ostream& strm, Logger::SeverityLevels level);

/** The function sets the logger attribute ClassName to the specified string and returns the initialized logger
* 
* @param classNameIn : input string name of the class
* 
* @returns : initialized BoostLogger
*/
BoostLogger setClassNameAttribute(const std::string& classNameIn);

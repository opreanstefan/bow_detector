#pragma once

#include "Exceptions.hpp"

template< typename T >
class EnumParser
{
private:
    std::map< std::string, T > m_parseMap;

public:
    EnumParser() {}

    T parseToEnum(const std::string& keyIn)
    {
        typename std::map < std::string, T >::const_iterator iValue = m_parseMap.find(keyIn);
        if (iValue  == m_parseMap.end())
            throw EnumParserError("There is no parsing for " + keyIn);
        return iValue->second;
    }
};

enum ExecutorVals
{
    TRAIN_BOW,
    TEST_BOW
};

template<> class EnumParser< ExecutorVals >
{
private:
    std::map< std::string, ExecutorVals > m_parseMap;

public:
    EnumParser()
    {
        m_parseMap.emplace("learn", TRAIN_BOW);
        m_parseMap.emplace("detect", TEST_BOW);
    }

    ExecutorVals parseToEnum(const std::string& keyIn)
    {
        std::map < std::string, ExecutorVals >::const_iterator iValue = m_parseMap.find(keyIn);
        if (iValue  == m_parseMap.end())
            throw EnumParserError("There is no parsing for " + keyIn);
        return iValue->second;
    }
};

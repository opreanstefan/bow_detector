#pragma once

#include "Config/CConfigFile.hpp"
#include "IRunner.hpp"
#include "Tools/Logger/CLogger.hpp"
#include "Types.hpp"

class BOWLearner : public IRunner
{
    static BoostLogger sm_lg;

    DetectorPtr m_detector;
    ExtractorPtr m_extractor;
    MatcherPtr m_matcher;

    cv::BOWKMeansTrainer m_bowTrainer;

    std::string m_outputFolder;

    void extractBOWTrainer(const PathsVector& imagesPathsIn);

public:
    BOWLearner(ConfigFile& configFileIn);

    void execute();
};

#pragma once

#include "Config/CConfigFile.hpp"
#include "IRunner.hpp"
#include "Tools/Logger/CLogger.hpp"
#include "Types.hpp"

class BOWDetector : public IRunner
{
private:
    static BoostLogger sm_lg;

    DetectorPtr m_detector;
    ExtractorPtr m_extractor;
    MatcherPtr m_matcher;
    cv::BOWImgDescriptorExtractor m_bowDE;

    fs::path m_imagesFolder;

    // TODO: add function to say if to convert to 8U the vocabulary
    //bool isUCharDescriptors();
    // TODO: better tp add a class of BOWKMajorityTrainer
public:
    BOWDetector(ConfigFile& configFileIn);

    void execute();
};

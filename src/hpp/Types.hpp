#pragma once

#include <boost/filesystem.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>

enum RetVals
{
    NO_ERRORS = 0,
    HELP,
    EXTERNAL_EXCEPTION,
    CPP_EXCEPTION,
    UNKNOWN_EXCEPTION
};

namespace fs = boost::filesystem;
namespace xml_parser = boost::property_tree::xml_parser;

typedef boost::property_tree::ptree PTree;

typedef cv::Ptr< cv::BOWImgDescriptorExtractor > BOWExtractorPtr;
typedef cv::Ptr< cv::DescriptorExtractor > ExtractorPtr;
typedef cv::Ptr< cv::DescriptorMatcher > MatcherPtr;
typedef cv::Ptr< cv::FeatureDetector > DetectorPtr;

typedef std::vector< fs::path > PathsVector;
typedef std::vector< cv::KeyPoint > KeyPointsVerctor;
